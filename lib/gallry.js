'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/* global define self */
(function () {
  var className = 'gallery-lightbox';
  var style = '\n    .' + className + ' {\n      position: fixed;\n      top: 0;\n      bottom: 0;\n      left: 0;\n      right: 0;\n      background: rgba(0, 0, 0, 0.9);\n      opacity: 0;\n      visibility: hidden;\n      transition: opacity .3s ease, visibility .3s ease;\n      display: flex;\n      flex-direction: column;\n      align-items: center;\n      justify-content: center;\n      font-size: 16px;\n      font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;\n      padding: 1em;\n      z-index: 9999;\n    }\n    .' + className + '.-open {\n      opacity: 1;\n      visibility: visible;\n    }\n    .' + className + ' > .description {\n      margin-top: 1em;\n      color: white;\n      -webkit-font-smoothing: antialiased;\n    }\n    .' + className + ' > .image,\n    .' + className + ' > .image > img {\n      max-width: 100%;\n      max-height: 100%;\n    }\n    .' + className + ' > .image {\n      max-width: calc(100% - 6em);\n    }\n    .' + className + ' > .next,\n    .' + className + ' > .prev,\n    .' + className + ' > .close {\n      position: absolute;\n      width: 3em;\n      height: 3em;\n      padding: 0;\n      margin: 0;\n      background: rgba(0, 0, 0, 0.2);\n      border: 0;\n      cursor: pointer;\n    }\n    .' + className + ' > .close {\n      top: 1%;\n      right: 1%;\n      font-size: 0.85em;\n    }\n    .' + className + ' > .close:before,\n    .' + className + ' > .close:after,\n    .' + className + ' > .prev:before,\n    .' + className + ' > .next:before,\n    .' + className + ' > .prev:after,\n    .' + className + ' > .next:after {\n      content: \'\';\n      width: 80%;\n      height: 2px;\n      position: absolute;\n      top: 50%;\n      left: 10%;\n      background: white;\n      transform-origin: center;\n    }\n    .' + className + ' > .close:before {\n      transform: translateY(-1px) rotate(45deg);\n    }\n    .' + className + ' > .close:after {\n      transform: translateY(-1px) rotate(-45deg);\n    }\n    .' + className + ' > .next,\n    .' + className + ' > .prev {\n      position: absolute;\n      width: 7.5%;\n      height: 7.5%;\n      top: 50%;\n      transform: translateY(-1em)\n    }\n    .' + className + ' > .next {\n      right: 2em;\n    }\n    .' + className + ' > .prev {\n      left: 2em;\n    }\n    .' + className + ' > .prev:before,\n    .' + className + ' > .next:before,\n    .' + className + ' > .prev:after,\n    .' + className + ' > .next:after {\n      width: 20%;\n      left: 40%;\n    }\n    .' + className + ' > .next:before,\n    .' + className + ' > .next:after {\n      transform-origin: right;\n    }\n    .' + className + ' > .prev:before,\n    .' + className + ' > .prev:after {\n      transform-origin: left;\n    }\n    .' + className + ' > .next:after,\n    .' + className + ' > .prev:after {\n      transform: rotate(45deg);\n    }\n    .' + className + ' > .next:before,\n    .' + className + ' > .prev:before {\n      transform: rotate(-45deg);\n    }\n    @media screen and (max-width: 600px) {\n      .' + className + ' > .next,\n      .' + className + ' > .prev {\n        position: absolute;\n        width: 40%;\n        height: 10%;\n        top: 50%;\n        z-index: 999;\n      }\n    }\n  ';
  var currentGallery = void 0;

  // Setup elements
  var lightbox = document.createElement('div');
  lightbox.className = className;

  var closeButton = document.createElement('button');
  closeButton.className = 'close';
  lightbox.appendChild(closeButton);

  var prevButton = document.createElement('button');
  prevButton.className = 'prev';
  lightbox.appendChild(prevButton);

  var nextButton = document.createElement('button');
  nextButton.className = 'next';
  lightbox.appendChild(nextButton);

  var currentImageWrapper = document.createElement('div');
  currentImageWrapper.className = 'image';
  lightbox.appendChild(currentImageWrapper);

  var currentImage = document.createElement('img');
  currentImageWrapper.appendChild(currentImage);

  var description = document.createElement('div');
  description.className = 'description';
  lightbox.appendChild(description);

  var styleDiv = document.createElement('style');
  styleDiv.innerHTML = style;

  document.body.appendChild(lightbox);
  document.body.appendChild(styleDiv);

  // Setup methods
  var openImage = function openImage(image, element) {
    lightbox.className = className + ' -open';
    currentImage.src = image.getAttribute('data-gallery-src');
    description.innerHTML = image.getAttribute('data-gallery-desc') || '';
    element.dispatchEvent(new CustomEvent('changeGallery', {
      bubbles: true
    }));
  };

  var closeGallery = function closeGallery(element) {
    lightbox.className = className;
    currentGallery = null;
    element.dispatchEvent(new CustomEvent('closeGallery', {
      bubbles: true
    }));
  };

  var createGallery = function createGallery(images) {
    var galleryImages = [];

    for (var index in images) {
      galleryImages[index] = images[index];
    }

    var _gallery = {
      currentIndex: 0,
      next: function next(element) {
        _gallery.currentIndex = (_gallery.currentIndex + 1) % galleryImages.length;
        openImage(galleryImages[_gallery.currentIndex], element);
      },
      prev: function prev(element) {
        if (_gallery.currentIndex == 0) {
          _gallery.currentIndex = galleryImages.length - 1;
        } else {
          _gallery.currentIndex -= 1;
        }

        openImage(images[_gallery.currentIndex], element);
      }
    };

    var onClick = function onClick(index, element) {
      currentGallery = _gallery;
      _gallery.currentIndex = index;
      openImage(galleryImages[index], element);
    };

    var _loop = function _loop(i) {
      galleryImages[i].addEventListener('click', function (event) {
        onClick(i, event.target);
      });
    };

    for (var i = 0; i < galleryImages.length; i++) {
      _loop(i);
    }
  };

  window.addEventListener('keyup', function (event) {
    if (document.querySelector('.' + className).classList.contains('-open')) {
      if (event.keyCode === 27) {
        closeButton.click();
      }

      if (currentGallery) {
        if (event.keyCode === 37) {
          prevButton.click();
        }

        if (event.keyCode === 39) {
          nextButton.click();
        }
      }
    }
  });

  document.querySelector('.gallery-lightbox').addEventListener('click', function (event) {
    if (event.target.nodeName == 'DIV') {
      closeGallery(event.target);
    }
  });

  closeButton.addEventListener('click', function (event) {
    closeGallery(event.target);
  });

  prevButton.addEventListener('click', function (event) {
    return currentGallery && currentGallery.prev(event.target);
  });

  nextButton.addEventListener('click', function (event) {
    return currentGallery && currentGallery.next(event.target);
  });

  // Create galleries
  var initGallery = function initGallery(parent) {
    var images = parent.querySelectorAll('[data-gallery-src]');
    var galleries = {};
    for (var i = 0; i < images.length; i++) {
      var galleryId = images[i].getAttribute('data-gallery-id') || '_';
      galleries[galleryId] = (galleries[galleryId] || []).concat(images[i]);
    }
    Object.keys(galleries).forEach(function (k) {
      return createGallery(galleries[k]);
    });
  };

  // Umd
  (function (root, factory) {
    if (typeof define === 'function' && define.amd) {
      // AMD. Register as an anonymous module.
      define([], factory);
    } else if ((typeof exports === 'undefined' ? 'undefined' : _typeof(exports)) === 'object') {
      module.exports = factory();
    } else {
      // Browser globals
      root.initGallery = factory();
    }
  })(typeof self !== 'undefined' ? self : this, function () {
    return initGallery;
  });

  initGallery(document);
})();
