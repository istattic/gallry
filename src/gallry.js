/* global define self */
(function () {
  const className = 'gallery-lightbox'
  const style = `
    .${className} {
      position: fixed;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      background: rgba(0, 0, 0, 0.9);
      opacity: 0;
      visibility: hidden;
      transition: opacity .3s ease, visibility .3s ease;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      font-size: 16px;
      font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
      padding: 1em;
      z-index: 9999;
    }
    .${className}.-open {
      opacity: 1;
      visibility: visible;
    }
    .${className} > .description {
      margin-top: 1em;
      color: white;
      -webkit-font-smoothing: antialiased;
    }
    .${className} > .image,
    .${className} > .image > img {
      max-width: 100%;
      max-height: 100%;
    }
    .${className} > .image {
      max-width: calc(100% - 6em);
    }
    .${className} > .next,
    .${className} > .prev,
    .${className} > .close {
      position: absolute;
      width: 3em;
      height: 3em;
      padding: 0;
      margin: 0;
      background: rgba(0, 0, 0, 0.2);
      border: 0;
      cursor: pointer;
    }
    .${className} > .close {
      top: 1%;
      right: 1%;
      font-size: 0.85em;
    }
    .${className} > .close:before,
    .${className} > .close:after,
    .${className} > .prev:before,
    .${className} > .next:before,
    .${className} > .prev:after,
    .${className} > .next:after {
      content: '';
      width: 80%;
      height: 2px;
      position: absolute;
      top: 50%;
      left: 10%;
      background: white;
      transform-origin: center;
    }
    .${className} > .close:before {
      transform: translateY(-1px) rotate(45deg);
    }
    .${className} > .close:after {
      transform: translateY(-1px) rotate(-45deg);
    }
    .${className} > .next,
    .${className} > .prev {
      position: absolute;
      width: 7.5%;
      height: 7.5%;
      top: 50%;
      transform: translateY(-1em)
    }
    .${className} > .next {
      right: 2em;
    }
    .${className} > .prev {
      left: 2em;
    }
    .${className} > .prev:before,
    .${className} > .next:before,
    .${className} > .prev:after,
    .${className} > .next:after {
      width: 20%;
      left: 40%;
    }
    .${className} > .next:before,
    .${className} > .next:after {
      transform-origin: right;
    }
    .${className} > .prev:before,
    .${className} > .prev:after {
      transform-origin: left;
    }
    .${className} > .next:after,
    .${className} > .prev:after {
      transform: rotate(45deg);
    }
    .${className} > .next:before,
    .${className} > .prev:before {
      transform: rotate(-45deg);
    }
    @media screen and (max-width: 600px) {
      .${className} > .next,
      .${className} > .prev {
        position: absolute;
        width: 40%;
        height: 10%;
        top: 50%;
        z-index: 999;
      }
    }
  `
  let currentGallery

  // Setup elements
  const lightbox = document.createElement('div')
  lightbox.className = className

  const closeButton = document.createElement('button')
  closeButton.className = 'close'
  lightbox.appendChild(closeButton)

  const prevButton = document.createElement('button')
  prevButton.className = 'prev'
  lightbox.appendChild(prevButton)

  const nextButton = document.createElement('button')
  nextButton.className = 'next'
  lightbox.appendChild(nextButton)

  const currentImageWrapper = document.createElement('div')
  currentImageWrapper.className = 'image'
  lightbox.appendChild(currentImageWrapper)

  const currentImage = document.createElement('img')
  currentImageWrapper.appendChild(currentImage)

  const description = document.createElement('div')
  description.className = 'description'
  lightbox.appendChild(description)

  const styleDiv = document.createElement('style')
  styleDiv.innerHTML = style

  document.body.appendChild(lightbox)
  document.body.appendChild(styleDiv)

  // Setup methods
  const openImage = (image, element) => {
    lightbox.className = `${className} -open`
    currentImage.src = image.getAttribute('data-gallery-src')
    description.innerHTML = image.getAttribute('data-gallery-desc') || ''
    element.dispatchEvent(new CustomEvent('changeGallery', {
        bubbles: true
    }))
  }

  const closeGallery = (element) => {
    lightbox.className = className
    currentGallery = null
    element.dispatchEvent(new CustomEvent('closeGallery', {
        bubbles: true
    }))
  }

  const createGallery = (images) => {
    const galleryImages = []

    for(let index in images) {
      galleryImages[index] = images[index]
    }

    const _gallery = {
      currentIndex: 0,
      next: (element) => {
        _gallery.currentIndex = (_gallery.currentIndex + 1) % galleryImages.length
        openImage(galleryImages[_gallery.currentIndex], element)
      },
      prev: (element) => {
        if (_gallery.currentIndex == 0) {
          _gallery.currentIndex = galleryImages.length - 1
        } else {
          _gallery.currentIndex -= 1
        }

        openImage(images[_gallery.currentIndex], element)
      }
    }

    const onClick = (index, element) => {
      currentGallery = _gallery
      _gallery.currentIndex = index
      openImage(galleryImages[index], element)
    }

    for (let i = 0; i < galleryImages.length; i++) {
      galleryImages[i].addEventListener('click', (event) => {
        onClick(i, event.target)
      })
    }
  }

  window.addEventListener('keyup', (event) => {
    if (document.querySelector('.' + className).classList.contains('-open')) {
      if (event.keyCode === 27) {
        closeButton.click()
      }

      if (currentGallery) {
        if (event.keyCode === 37) {
          prevButton.click()
        }

        if (event.keyCode === 39) {
          nextButton.click()
        }
      }
    }
  })

  document.querySelector('.gallery-lightbox').addEventListener('click', (event) => {
    if (event.target.nodeName == 'DIV') {
      closeGallery(event.target)
    }
  })

  closeButton.addEventListener('click', (event) => {
    closeGallery(event.target)
  })

  prevButton.addEventListener('click', (event) => (
    currentGallery && currentGallery.prev(event.target)
  ))

  nextButton.addEventListener('click', (event) => (
    currentGallery && currentGallery.next(event.target)
  ))

  // Create galleries
  const initGallery = (parent) => {
    let images = parent.querySelectorAll('[data-gallery-src]')
    let galleries = {}
    for (let i = 0; i < images.length; i++) {
      let galleryId = images[i].getAttribute('data-gallery-id') || '_'
      galleries[galleryId] = (galleries[galleryId] || []).concat(images[i])
    }
    Object.keys(galleries).forEach(k => createGallery(galleries[k]))
  }

  // Umd
  (function (root, factory) {
    if (typeof define === 'function' && define.amd) {
      // AMD. Register as an anonymous module.
      define([], factory)
    } else if (typeof exports === 'object') {
      module.exports = factory()
    } else {
      // Browser globals
      root.initGallery = factory()
    }
  }(typeof self !== 'undefined' ? self : this, () => initGallery))

  initGallery(document)
}())
